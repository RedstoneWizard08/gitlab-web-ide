import { GitLabApiClient } from '@gitlab/vscode-mediator-commands';
import { getCompletionToken } from './tokenManagement';
import { log } from './log';

jest.mock('./log');

// We'll use toBe assertion so it doesn't need to be a real token
const TEST_TOKEN = {
  token: 'abc',
  expires_in: 1,
  created_at: 1,
};
const TEST_ERROR = new Error('Big bad error!');

describe('tokenManagement', () => {
  describe('getCompletionToken', () => {
    let client: GitLabApiClient;
    describe('when success', () => {
      beforeEach(() => {
        client = {
          fetchFromApi: jest.fn(),
        };
        jest.mocked(client.fetchFromApi).mockResolvedValue(TEST_TOKEN);
      });

      it('calls command and returns token', async () => {
        const token = await getCompletionToken(client);

        expect(client.fetchFromApi).toHaveBeenCalledTimes(1);
        expect(client.fetchFromApi).toHaveBeenCalledWith({
          path: 'code_suggestions/tokens',
          type: 'api',
          method: 'POST',
        });
        expect(token).toBe(TEST_TOKEN);
      });

      it('does not log an error', async () => {
        await getCompletionToken(client);

        expect(log.error).not.toHaveBeenCalled();
      });
    });

    describe('when command fails', () => {
      beforeEach(() => {
        client = {
          fetchFromApi: jest.fn(),
        };
        jest.mocked(client.fetchFromApi).mockRejectedValue(TEST_ERROR);
      });

      it('returns undefined', async () => {
        const token = await getCompletionToken(client);

        expect(token).toBeUndefined();
      });

      it('logs the error', async () => {
        await getCompletionToken(client);

        expect(log.error).toHaveBeenCalledWith('Failed to obtain completion token.', TEST_ERROR);
      });
    });
  });
});
