import * as vscode from 'vscode';
import { COMMAND_FETCH_PROJECT, FetchProjectCommand } from '@gitlab/vscode-mediator-commands';
import { memoize } from 'lodash';

const fetchProjectInternal: FetchProjectCommand = () =>
  vscode.commands.executeCommand(COMMAND_FETCH_PROJECT);

export const fetchProject = memoize(fetchProjectInternal);
