import { GitLabApiClient, PostRequest } from '@gitlab/vscode-mediator-commands';
import { log } from './log';

export interface CompletionToken {
  access_token: string;
  /* expires in number of seconds since `created_at` */
  expires_in: number;
  /* unix timestamp of the datetime of token creation */
  created_at: number;
}

const tokenRequest: PostRequest<CompletionToken> = {
  type: 'api',
  method: 'POST',
  path: 'code_suggestions/tokens',
};

export const getCompletionToken: (
  client: GitLabApiClient,
) => Promise<CompletionToken | undefined> = async client => {
  try {
    return await client.fetchFromApi(tokenRequest);
  } catch (e) {
    log.error(`Failed to obtain completion token.`, e as Error);

    return undefined;
  }
};
