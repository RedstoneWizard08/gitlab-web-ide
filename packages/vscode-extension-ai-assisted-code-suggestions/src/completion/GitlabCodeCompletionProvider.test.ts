import * as vscode from 'vscode';
import { COMMAND_FETCH_PROJECT } from '@gitlab/vscode-mediator-commands';
import {
  GitLabCodeCompletionProvider,
  CIRCUIT_BREAK_INTERVAL_MS,
} from './GitlabCodeCompletionProvider';
import { log, initializeLogging } from '../log';
import { fetchProject } from '../utils/fetchProject';
import { CompletionToken } from '../tokenManagement';

jest.mock('../log');

const TEST_TOKEN: CompletionToken = {
  access_token: 'foobar',
  expires_in: 10,
  created_at: new Date('20230523T00:01:00Z').getTime(),
};

describe('GitLabCodeCompletionProvider', () => {
  // const mockPrompt = 'const areaOfCube = ';
  const mockPrompt = {
    prompt_version: 1,
    project_path: 'gitlab-org/modelops/applied-ml/review-recommender/pipeline-scheduler',
    project_id: 33191677,
    current_file: {
      file_name: 'circleMath.js',
      content_above_cursor: 'const areaOfCube = ',
      content_below_cursor: '',
    },
  };
  const mockEmptyDocument: Partial<vscode.TextDocument> = {
    getText: () => '',
    lineAt: () => ({ text: '' } as vscode.TextLine),
    fileName: 'circleMath.js',
  };
  const mockText = mockPrompt.current_file.content_above_cursor;
  const mockDocumentParial: Partial<vscode.TextDocument> = {
    getText: () => mockText,
    lineAt: () => ({ text: mockText } as vscode.TextLine),
    fileName: 'circleMath.js',
  };
  const mockDocument = mockDocumentParial as unknown as vscode.TextDocument;
  const mockPosition = {
    line: 0,
    character: mockText.length,
  } as vscode.Position;
  const expectedServer = 'https://codesuggestions.gitlab.com/v2/completions';
  const choice = '(side) => ';
  const mockCompletions = { choices: [{ text: choice }] };
  const mockInlineCompletions = [] as vscode.InlineCompletionItem[];

  beforeEach(() => {
    fetchProject.cache.clear?.();

    // Used by 'fetchProject' - we'll go ahead and test a bit in integration here
    jest.mocked(vscode.commands.executeCommand).mockResolvedValue({
      id: mockPrompt.project_id,
      path_with_namespace: mockPrompt.project_path,
    });
  });

  describe('fetchCompletions', () => {
    const mockFetchResponse = {
      ok: true,
      status: 200,
      json: () => Promise.resolve(mockCompletions),
    } as Response;
    const mockFetchFailureResponse = {
      ok: false,
      status: 401,
      json: () => Promise.resolve(mockCompletions),
      text: () => Promise.resolve(choice),
      url: 'http://example.org',
    } as Response;
    const fetchParams = {
      model: 'gitlab',
      prompt: mockPrompt,
      stop: [],
    };

    it('calls fetch and returns data', async () => {
      global.fetch = jest.fn().mockResolvedValue(mockFetchResponse);

      const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);
      const data = await glcp.fetchCompletions(fetchParams);
      expect(fetch).toHaveBeenCalledWith(expectedServer, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${TEST_TOKEN.access_token}`,
          'Content-Type': 'application/json',
          'X-Gitlab-Authentication-Type': 'oidc',
        },
        body: JSON.stringify(fetchParams),
      });
      expect(data).toEqual(mockCompletions);
    });

    it('when fetch failures occur', async () => {
      global.fetch = jest.fn().mockResolvedValue(mockFetchFailureResponse);
      global.console.warn = jest.fn();

      const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);
      await expect(glcp.fetchCompletions(fetchParams)).rejects.toThrowError(
        /Fetching code suggestions .* failed/,
      );
      expect(fetch).toHaveBeenCalledWith(expectedServer, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${TEST_TOKEN.access_token}`,
          'Content-Type': 'application/json',
          'X-Gitlab-Authentication-Type': 'oidc',
        },
        body: JSON.stringify(fetchParams),
      });
    });
  });

  describe('getCompletions', () => {
    it('when no prompt returns empty array', async () => {
      const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);
      glcp.fetchCompletions = jest.fn().mockResolvedValue(mockCompletions);

      const inlineCompletions = await glcp.getCompletions(
        mockEmptyDocument as unknown as vscode.TextDocument,
        mockPosition,
      );

      expect(inlineCompletions).toEqual([]);
    });

    it('with a prompt, returns completions', async () => {
      const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);
      glcp.fetchCompletions = jest.fn().mockResolvedValue(mockCompletions);

      const inlineCompletions = await glcp.getCompletions(
        mockDocument as unknown as vscode.TextDocument,
        mockPosition,
      );

      expect(inlineCompletions[0].insertText).toEqual('(side) => ');
    });

    it('only fetches project once', async () => {
      const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);
      glcp.fetchCompletions = jest.fn().mockResolvedValue(mockCompletions);

      await glcp.getCompletions(mockDocument as vscode.TextDocument, mockPosition);
      await glcp.getCompletions(mockDocument as vscode.TextDocument, mockPosition);
      await glcp.getCompletions(mockDocument as vscode.TextDocument, mockPosition);

      expect(jest.mocked(vscode.commands.executeCommand).mock.calls).toEqual([
        [COMMAND_FETCH_PROJECT],
      ]);
    });

    it('logs a fetch error', async () => {
      initializeLogging(() => jest.fn());
      log.error = jest.fn();
      const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);
      glcp.fetchCompletions = jest.fn().mockRejectedValue(new Error('test problem'));

      await glcp.getCompletions(mockDocument, mockPosition);

      expect(log.error).toHaveBeenCalledWith(
        'Error obtaining suggestions:',
        new Error('test problem'),
      );
    });

    describe(`circuit breaking`, () => {
      const turnOnCircuitBreaker = async (glcp: GitLabCodeCompletionProvider) => {
        await glcp.getCompletions(mockDocument, mockPosition);
        await glcp.getCompletions(mockDocument, mockPosition);
        await glcp.getCompletions(mockDocument, mockPosition);
        await glcp.getCompletions(mockDocument, mockPosition);
      };

      it(`starts breaking after 4 errors`, async () => {
        const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);

        glcp.fetchCompletions = jest.fn().mockRejectedValue(new Error('test problem'));

        await turnOnCircuitBreaker(glcp);

        glcp.fetchCompletions = jest.fn().mockResolvedValue(mockCompletions);

        const result = await glcp.getCompletions(mockDocument, mockPosition);
        expect(result).toEqual([]);
        expect(glcp.fetchCompletions).not.toHaveBeenCalled();
      });

      describe("after circuit breaker's break time elapses", () => {
        it('fetches completions again', async () => {
          const glcp = new GitLabCodeCompletionProvider(TEST_TOKEN);
          glcp.fetchCompletions = jest.fn().mockRejectedValue(new Error('test problem'));

          await turnOnCircuitBreaker(glcp);

          jest.useFakeTimers().setSystemTime(new Date(Date.now() + CIRCUIT_BREAK_INTERVAL_MS));

          glcp.fetchCompletions = jest.fn().mockResolvedValue(mockCompletions);

          await glcp.getCompletions(mockDocument, mockPosition);

          expect(glcp.fetchCompletions).toHaveBeenCalled();
        });
      });
    });
  });

  describe('provideInlineCompletionItems', () => {
    const mockContext = {
      triggerKind: vscode.InlineCompletionTriggerKind.Automatic,
    } as vscode.InlineCompletionContext;
    jest.useFakeTimers();

    // Jest timers with multiple async resolvers https://github.com/facebook/jest/issues/11876
    it('provides inline completions', async () => {
      const glcp: GitLabCodeCompletionProvider = new GitLabCodeCompletionProvider(TEST_TOKEN, true);
      glcp.getCompletions = jest.fn().mockResolvedValue(mockInlineCompletions);

      await glcp.provideInlineCompletionItems(
        mockDocument as unknown as vscode.TextDocument,
        mockPosition,
        mockContext,
      );

      expect(glcp.getCompletions).toHaveBeenCalled();
    });
  });
});
