import { GitLabApiClient, COMMAND_FETCH_FROM_API } from '@gitlab/vscode-mediator-commands';
import * as vscode from 'vscode';

// TODO: extract this implementation to a new package
// vscode-mediator-api-client
export const mediatorApiClient: GitLabApiClient = {
  fetchFromApi: async request => vscode.commands.executeCommand(COMMAND_FETCH_FROM_API, request),
};
