export const prettyJson = (obj: Record<string, unknown> | unknown[]): string =>
  JSON.stringify(obj, null, 2);

export interface DetailedError extends Error {
  readonly details: Record<string, unknown>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isDetailedError(object: any): object is DetailedError {
  return Boolean(object.details);
}
