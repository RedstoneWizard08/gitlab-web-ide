import { escapeCssQuotedValue } from './escapeCssQuotedValue';

describe('escapeCssQuotedValue', () => {
  it('escapes single and double quotes', () => {
    expect(escapeCssQuotedValue(`http://comma/'"`)).toBe('http://comma/%27%22');
  });
});
