import * as vscode from 'vscode';
import { generateBranchName } from './generateBranchName';
import { IBranchSelection, promptBranchName } from './promptBranchName';

jest.mock('./generateBranchName');

const TEST_BRANCH = 'test-main';
const TEST_GENERATED_BRANCH = 'test-main-path-123';

describe('scm/commit/promptBranchName', () => {
  let resolveInputBox: (value: string | undefined) => void;
  let result: Promise<IBranchSelection | undefined>;

  beforeEach(() => {
    jest.spyOn(vscode.window, 'showInputBox').mockImplementation(
      () =>
        new Promise(resolve => {
          resolveInputBox = resolve;
        }),
    );

    jest.mocked(generateBranchName).mockReturnValue(TEST_GENERATED_BRANCH);

    result = promptBranchName(TEST_BRANCH);
  });

  it('shows input box requesting a branch name', () => {
    expect(vscode.window.showInputBox).toHaveBeenCalledWith({
      ignoreFocusOut: true,
      placeHolder: `Leave empty to use "${TEST_GENERATED_BRANCH}"`,
      title: 'Create a new branch',
    });
  });

  it.each`
    inputBoxResult        | expectation
    ${undefined}          | ${undefined}
    ${''}                 | ${{ isNewBranch: true, branchName: TEST_GENERATED_BRANCH }}
    ${'brand-new-branch'} | ${{ isNewBranch: true, branchName: 'brand-new-branch' }}
  `(
    'with inputBox resolves with $inputBoxResult, returns $expectation',
    async ({ inputBoxResult, expectation }) => {
      resolveInputBox(inputBoxResult);

      await expect(result).resolves.toEqual(expectation);
    },
  );
});
