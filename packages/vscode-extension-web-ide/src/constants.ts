export const FULL_EXTENSION_ID = 'gitlab.gitlab-web-ide';
export const EXTENSION_ID = 'gitlab-web-ide';

export const FS_SCHEME = 'gitlab-web-ide';
export const SCM_SCHEME = 'gitlab-web-ide-scm';
export const MR_SCHEME = 'gitlab-web-ide-mr';

export const GET_STARTED_WALKTHROUGH_ID = `${FULL_EXTENSION_ID}#getStartedWebIde`;

// region: commands ----------------------------------------------------

export const COMMIT_COMMAND_ID = `${EXTENSION_ID}.commit`;
export const COMMIT_COMMAND_TEXT = 'Commit';

export const SECONDARY_COMMIT_COMMAND_TEXT = 'Commit to new branch';

export const CHECKOUT_BRANCH_COMMAND_ID = `${EXTENSION_ID}.checkout-branch`;

export const COMPARE_WITH_MR_BASE_COMMAND_ID = `${EXTENSION_ID}.compare-with-mr-base`;

export const GO_TO_GITLAB_COMMAND_ID = `${EXTENSION_ID}.go-to-gitlab`;

export const GO_TO_PROJECT_COMMAND_ID = `${EXTENSION_ID}.go-to-project`;

export const START_REMOTE_COMMAND_ID = `${EXTENSION_ID}.start-remote`;

export const SHARE_YOUR_FEEDBACK_COMMAND_ID = `${EXTENSION_ID}.share-your-feedback`;

// region: internal commands -------------------------------------------

export const CREATE_BRANCH_COMMAND_ID = `${EXTENSION_ID}.create-branch`;

export const OPEN_REMOTE_WINDOW_COMMAND_ID = `${EXTENSION_ID}.open-remote-window`;

export const RELOAD_COMMAND_ID = `${EXTENSION_ID}.reload`;

export const RELOAD_WITH_WARNING_COMMAND_ID = `${EXTENSION_ID}.reload-with-warning`;

// region: source control-----------------------------------------------

export const SOURCE_CONTROL_CHANGES_ID = 'changes';
export const SOURCE_CONTROL_CHANGES_NAME = 'Changes';

export const SOURCE_CONTROL_ID = 'web-ide';
export const SOURCE_CONTROL_NAME = 'Web IDE';

// region: UI elements --------------------------------------------------

export const BRANCH_STATUS_BAR_ITEM_ID = 'branchStatusBarItem';
export const BRANCH_STATUS_BAR_ITEM_PRIORITY = 50;

// region: Context
export const WEB_IDE_READY_CONTEXT_ID = 'gitlab-web-ide.is-ready';
export const MERGE_REQUEST_FILE_PATHS_CONTEXT_ID = 'gitlab-web-ide.mergeRequestFilePaths';

// region: Micellaneous -------------------------------------------------------
export const FEEDBACK_ISSUE_URL = 'https://gitlab.com/gitlab-org/gitlab/-/issues/385787';

// region: Local storage ------------------------------------------------------
export const COMMIT_TO_DEFAULT_BRANCH_PREFERENCE = 'commit-to-default-branch';
