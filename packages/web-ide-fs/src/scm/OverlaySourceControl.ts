import { joinPaths } from '@gitlab/utils-path';
import { mapKeys } from 'lodash';
import { FileFlag } from 'browserfs/dist/node/core/file_flag';
import { existsAsFile, readAllFiles } from '../browserfs/utils';
import { FileStatusType, IFileStatus, ISourceControlSystem } from '../types';
import { IReadonlyPromisifiedBrowserFS } from '../browserfs/types';
import { IDeletedFilesLogReadonly } from '../browserfs/typesOverlayFS';
import { readdirRecursive } from '../browserfs/utils/readdirRecursive';

interface OverlaySourceControlOptions {
  readonly writable: IReadonlyPromisifiedBrowserFS;
  readonly readable: IReadonlyPromisifiedBrowserFS;
  readonly deletedFilesLog: IDeletedFilesLogReadonly;
  readonly repoRootPath: string;
}

export class OverlaySourceControl implements ISourceControlSystem {
  private readonly _readableFS: IReadonlyPromisifiedBrowserFS;

  private readonly _writableFS: IReadonlyPromisifiedBrowserFS;

  private readonly _deletedFilesLog: IDeletedFilesLogReadonly;

  private readonly _repoRootPath: string;

  constructor(opt: OverlaySourceControlOptions) {
    this._readableFS = opt.readable;
    this._writableFS = opt.writable;
    this._deletedFilesLog = opt.deletedFilesLog;

    // Force leading slash
    this._repoRootPath = joinPaths('/', opt.repoRootPath);
  }

  async status(): Promise<IFileStatus[]> {
    // 1. Read `deletedFiles`. Treat each entry as "deleted" file statuses.
    // 2. Recursively read all files in the `repoRootPath` in `writable`. For each file:
    //     a. If file was assumed to be deleted (based on `.deletedFiles.log`), remove "deleted" file status.
    //     b. If file does not exist in `readable`, add "new" file status.
    //     c. ElseIf `writable` file content is different than `readable` file content, add "modified" file status.
    // 3. Return statuses.

    // Step 1
    const deletedFilesLog = await this._getDeletedFilesLog();

    // Step 2
    const touchedRepoFiles = await this._readAllWritableRepoFiles();
    const allTouchedPaths = new Set(Object.keys(touchedRepoFiles));
    const touchedStatuses = await this._processTouchedRepoFiles(touchedRepoFiles);

    const deletedStatuses: IFileStatus[] = deletedFilesLog
      .filter(deletedPath => !allTouchedPaths.has(deletedPath))
      .map(path => ({
        type: FileStatusType.Deleted,
        path,
      }));

    const statuses: IFileStatus[] = [...deletedStatuses, ...touchedStatuses];

    return statuses;
  }

  private async _processTouchedRepoFiles(
    touchedRepoFiles: Record<string, Buffer>,
  ): Promise<IFileStatus[]> {
    const newStatuses = await Promise.all(
      Object.entries(touchedRepoFiles).map(([path, content]) =>
        this._processTouchedRepoFile(path, content),
      ),
    );

    return newStatuses.flatMap(x => x);
  }

  private async _processTouchedRepoFile(path: string, content: Buffer): Promise<IFileStatus[]> {
    //     b. If file does not exist in `readable`, add "new" file status.
    //     c. ElseIf `writable` file content is different than `readable` file content, add "modified" file status.
    const fullPath = joinPaths(this._repoRootPath, path);

    const existsInReadable = await existsAsFile(fullPath, this._readableFS);

    if (!existsInReadable) {
      return [
        {
          type: FileStatusType.Created,
          path,
          content,
        },
      ];
    }

    const hasChanged = await this._fileContentHasChanged(fullPath, content);

    if (hasChanged) {
      return [
        {
          type: FileStatusType.Modified,
          path,
          content,
        },
      ];
    }

    return [];
  }

  private async _fileContentHasChanged(fullPath: string, content: Buffer): Promise<boolean> {
    // We can assume that the repo path exists here...
    // We also know this is a Buffer because we do not pass encoding
    const oldContent = <Buffer>(
      await this._readableFS.readFile(fullPath, null, FileFlag.getFileFlag('r'))
    );

    return !content.equals(oldContent);
  }

  private async _readAllWritableRepoFiles(): Promise<Record<string, Buffer>> {
    const hasRepoFiles = await this._writableFS.exists(this._repoRootPath);

    if (!hasRepoFiles) {
      return {};
    }

    const result = await readAllFiles(this._writableFS, this._repoRootPath);

    return mapKeys(result, (value, key) => this._cleanRepoFilePath(key));
  }

  private async _getDeletedFilesLog(): Promise<string[]> {
    const contents = await this._deletedFilesLog.getContents();

    if (!contents) {
      return [];
    }

    // what: From `contents.directories`, read all the files that we should consider deleted
    const deletedDirectories = Array.from(contents.directories.keys());
    const deletedDirectoriesFiles = await Promise.all(
      deletedDirectories.map(dir => readdirRecursive(this._readableFS, dir)),
    );
    const deletedFiles = new Set(deletedDirectoriesFiles.flatMap(x => x));

    // what: From `contents.files`, add the remaining files to our set
    contents.files.forEach(x => deletedFiles.add(x));

    return Array.from(deletedFiles).map(path => this._cleanRepoFilePath(path));
  }

  private _cleanRepoFilePath(entry: string): string {
    if (entry.startsWith(this._repoRootPath)) {
      return entry.slice(this._repoRootPath.length);
    }

    return entry;
  }
}
