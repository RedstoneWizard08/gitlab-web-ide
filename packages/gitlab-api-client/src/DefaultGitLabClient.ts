import { joinPaths } from '@gitlab/utils-path';
import { createResponseError } from './createResponseError';
import { ApiRequest, GetRequest, GitLabApiClient, PostRequest } from './types';

const withParams = (baseUrl: string, params: Record<string, string>) => {
  const paramEntries = Object.entries(params);

  if (!paramEntries.length) {
    return baseUrl;
  }

  const url = new URL(baseUrl);

  paramEntries.forEach(([key, value]) => {
    url.searchParams.append(key, value);
  });

  return url.toString();
};

export interface IDefaultGitLabClientConfig {
  baseUrl: string;
  authToken: string;
  httpHeaders?: Record<string, string>;
}

export class DefaultGitLabClient implements GitLabApiClient {
  private readonly _baseUrl: string;

  private readonly _httpHeaders: Record<string, string>;

  constructor(config: IDefaultGitLabClientConfig) {
    this._baseUrl = config.baseUrl;
    this._httpHeaders = {
      ...(config.httpHeaders || {}),
      ...(config.authToken ? { 'PRIVATE-TOKEN': config.authToken } : {}),
    };
  }

  async fetchFromApi<T>(request: ApiRequest<T>): Promise<T> {
    if (request.type === 'api' && request.method === 'GET') {
      return this.#makeGetRequest(request);
    }
    if (request.type === 'api' && request.method === 'POST') {
      return this.#makePostRequest(request);
    }
    throw new Error(`Unknown request: ${request.type} - ${request.method}`);
  }

  async #makePostRequest<T>(request: PostRequest<T>): Promise<T> {
    const url = this.#appendPathToBaseApiUrl(request.path);
    return this.#fetchPostJson(url, request.body);
  }

  async #makeGetRequest<T>(request: GetRequest<T>): Promise<T> {
    const url = this.#appendPathToBaseApiUrl(request.path);
    return this.#fetchGetJson(url, request.searchParams);
  }

  #appendPathToBaseApiUrl(path: string) {
    return joinPaths(this._baseUrl, 'api', 'v4', path);
  }

  async #fetchPostJson<TResponse, TBody>(url: string, body: TBody): Promise<TResponse> {
    const response = await fetch(url, {
      method: 'POST',
      body: body ? JSON.stringify(body) : undefined,
      headers: {
        ...this._httpHeaders,
        'Content-Type': 'application/json',
      },
    });

    if (!response.ok) {
      throw await createResponseError(response);
    }

    return <Promise<TResponse>>response.json();
  }

  async #fetchGetJson<T>(url: string, params: Record<string, string> = {}): Promise<T> {
    const response = await this.#fetchGetResponse(url, params);

    return (await response.json()) as T;
  }

  async #fetchGetResponse(url: string, params: Record<string, string> = {}): Promise<Response> {
    const response = await fetch(withParams(url, params), {
      method: 'GET',
      headers: this._httpHeaders,
    });

    if (!response.ok) {
      throw await createResponseError(response);
    }

    return response;
  }
}
